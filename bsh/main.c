#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

enum tokentype
{
    TK_WORD,
    TK_INFILE,
    TK_OUTFILE,
};

struct token
{
    char *start;
    size_t len;
    enum tokentype type;
    struct token *next;
};

struct tokenizer
{
    size_t token_count;
    struct token *head;
};

void tokenizer_init(struct tokenizer *tokenizer, char *line)
{
    tokenizer->token_count = 0;
    tokenizer->head = NULL;

    char *next = line;
    struct token *last_token = NULL;

    for (;;) {
        while (*next == ' ' || *next == '\t' || *next == '\n') {
            ++next;
        }

        if (*next == '\0') {
            break;
        }

        struct token *token = (struct token*)malloc(sizeof(struct token));
        token->start = next++;
        token->next = NULL;

        switch (*token->start) {
        case '<':
            token->type = TK_INFILE;
            break;
        case '>':
            token->type = TK_OUTFILE;
            break;
        default:
            token->type = TK_WORD;
            while (*next && *next != ' ' && *next != '\t' && *next != '\n') {
                ++next;
            }
        }

        token->len = next - token->start;

        if (last_token) {
            last_token->next = token;
        } else {
            tokenizer->head = token;
        }

        last_token = token;
        ++tokenizer->token_count;
    }
}

void tokenizer_free(struct tokenizer *tokenizer)
{
    struct token *next = tokenizer->head;
    while (next) {
        struct token *prev = next;
        next = next->next;
        free(prev);
    }
}

int get_user_line(char **line, size_t *maxlen)
{
    printf("$ ");
    return getline(line, maxlen, stdin);
}

int main()
{
    char *line = NULL;
    size_t maxlen = 0;
    setbuf(stdin, NULL); // Disable stdin buffering

    ssize_t len = 0;
    struct tokenizer tokenizer;

    while ((len = get_user_line(&line, &maxlen)) > 0) {

        tokenizer_init(&tokenizer, line);

        // Your code goes here

        tokenizer_free(&tokenizer);
    }
    if (line) {
        free(line);
    }

    printf("\n");

    return 0;
}
