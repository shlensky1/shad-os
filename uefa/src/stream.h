#pragma once

#include <stdio.h>

struct stream {
    FILE *infile;
    char *inbuf;
    size_t inbuflen;
    FILE *outfile;
    char *outbuf;
    size_t outbuflen;
    size_t outbufcap;
};

void stream_initialize(struct stream *stream);
void stream_free(struct stream *stream);
void stream_set_input_file(struct stream *stream, FILE* file);
void stream_set_input_buffer(struct stream *stream, char *buffer, size_t len);
void stream_set_output_file(struct stream *stream, FILE* file);
size_t stream_read(struct stream *stream, char *ptr, size_t len);
void stream_write(struct stream *stream, const char *ptr, size_t len);
char* stream_get_buffer(struct stream *stream, size_t *len);

char* read_file(FILE *file, size_t *len);
