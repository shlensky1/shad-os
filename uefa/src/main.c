#include "cli.h"
#include "plugin.h"
#include "encrypt.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#define PLUGINS_DIR "plugins/"

char* get_plugin_path(const char* exe_path, const char* name)
{
    size_t exe_path_len = strlen(exe_path);
    size_t name_len = strlen(name);
    size_t plugins_len = strlen(PLUGINS_DIR);
    char *path = malloc(exe_path_len + name_len + plugins_len + 3);
    strcpy(path, exe_path);
    while (exe_path_len && exe_path[exe_path_len - 1] != '/') {
        --exe_path_len;
    }
    strcpy(path + exe_path_len, PLUGINS_DIR);
    strcpy(path + exe_path_len + plugins_len, name);
    strcpy(path + exe_path_len + plugins_len + name_len, ".o");
    return path;
}

void encrypt_stdin(struct plugin *codec)
{
    size_t len;
    char *data = read_file(stdin, &len);
    encrypt(data, len);

    struct stream stream;
    stream_initialize(&stream);
    stream_set_input_buffer(&stream, data, len);
    stream_set_output_file(&stream, stdout);

    codec->encode(&stream);

    stream_free(&stream);
    free(data);
}

int decrypt_stdin(struct plugin *codec)
{
    int return_code = 0;
    struct stream stream;
    stream_initialize(&stream);
    stream_set_input_file(&stream, stdin);

    int code;
    if ((code = codec->decode(&stream)) != 0) {
        fprintf(stderr, "Decoding error (code %d)\n", code);
        return_code = 1;
        goto quit;
    }

    size_t len;
    char *data = stream_get_buffer(&stream, &len);
    decrypt(data, len);

    fwrite(data, 1, len, stdout);

quit:
    stream_free(&stream);
    return return_code;
}

int main(int argc, char *argv[])
{
    int return_code = 0;
    char *plugin_path = NULL;
    FILE *plugin_file = NULL;
    struct plugin *plugin = NULL;

    struct cli_args args = cli_parse(argc, argv);

    plugin_path = get_plugin_path(argv[0], args.codec);
    plugin_file = fopen(plugin_path, "r");
    if (plugin_file == NULL) {
        fprintf(stderr, "Error opening plugin file '%s': %s (errno %d)\n",
            plugin_path, strerror(errno), errno);
        return_code = 1;
        goto quit;
    }

    plugin = malloc(sizeof(struct plugin));
    int code;
    if ((code = plugin_load(plugin, plugin_file))) {
        fprintf(stderr, "Error loading plugin (code %d)\n", code);
        return_code = 1;
        goto quit;
    }

    if (args.encrypt) {
        encrypt_stdin(plugin);
    } else if ((code = decrypt_stdin(plugin)) != 0) {
        return_code = 1;
    }

    plugin_free(plugin);

quit:
    if (plugin != NULL) {
        free(plugin);
    }
    if (plugin_file != NULL) {
        fclose(plugin_file);
    }
    free(plugin_path);
    return return_code;
}
