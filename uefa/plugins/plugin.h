#include "stddef.h"

// Plugins only use `stream*` type as an opaque cookie.
struct stream;

// Write `len` bytes from `ptr` to stream. It is guaranteed that, when this function returns,
// all the bytes are successfully written.
void stream_write(struct stream *stream, const char *ptr, size_t len);

// Read `len` bytes to `ptr` from stream. Returns the number of bytes read. If returned number
// is less than `len`, then all further reads will return 0.
size_t stream_read(struct stream *stream, char *ptr, size_t len);

// The following functions should be implemented in a plugin.
void uefa_plugin_encode(struct stream *stream);
int uefa_plugin_decode(struct stream *stream); // on success, return 0; on failure, return nonzero
